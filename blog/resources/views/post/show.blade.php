@extends('layout.master')

@section('judul')
    Detail Pemain Film {{$cast->nama}}
@endsection

@section('content')
    <h1>Nama Pemain Film = {{$cast->nama}}</h1>
    <p>Umur = {{$cast->umur}}</p>
    <p>Biodata = {{$cast->bio}}</p>
@endsection