<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function home()
    {
        return view('layout.home');
    }

    public function create()
    {
        return view('post.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast')->with('success', 'Data Berhasil Ditambahkan');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('post.index', compact('cast'));
    }
    
    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('post.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('post.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                "nama" => $request["nama"],
                "umur" => $request["umur"],
                "bio" => $request["bio"]
            ]);
        return redirect('/cast')->with('success', 'Data Berhasil Diubah');
    }
        
    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast')->with('success', 'Data Berhasil Dihapus');
    }
}
